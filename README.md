# 20th October

A static site about Vietnamese Women's Day. [Go to page](https://20th-october.gitlab.io)

GPLv3 licensed. [Go to LICENSE](https://gitlab.com/20th-october/20th-october.gitlab.io/blob/master/LICENSE)
